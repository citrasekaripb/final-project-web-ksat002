import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://banksystem-demoshop.herokuapp.com/')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/a_Register'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Email'), 'voleg99892@cctyoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Password'), 'QJblfja5Cso=')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Confirm password'), 'QJblfja5Cso=')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/button_Register'))

WebUI.click(findTestObject('Object Repository/Page_DemoShop/button_Buy now'))

WebUI.click(findTestObject('Object Repository/Page_DemoShop/button_Pay by card'))

WebUI.click(findTestObject('Object Repository/Page_DemoShop/main_Card payment    Computer mouse    8.00_511f9b'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Card number_Number'), '2011762929693958')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/main_Card payment    Computer mouse    8.00_511f9b'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Expiration date_ExpiryDate'), '12/24')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/main_Card payment    Computer mouse    8.00_511f9b'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Cardholder name_Name'), 'citrasp')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/main_Card payment    Computer mouse    8.00_511f9b'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Security code_SecurityCode'), '440')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/button_Pay'))

String product = WebUI.getText(findTestObject('Object Repository/Page_DemoShop/h4_Computer mouse'))

WebUI.verifyMatch(product, 'Computer mouse', true)

String price = WebUI.getText(findTestObject('Object Repository/Page_DemoShop/p_8.00'))

WebUI.verifyMatch(price, '€8.00', true)

WebUI.closeBrowser()

