import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl('https://banksystem-demoshop.herokuapp.com/')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/link_Register'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Email'), 'figixat220@febeks.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Password'), 'QJblfja5Cso=')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Confirm password'), 'QJblfja5Cso=')

WebUI.click(findTestObject('Page_DemoShop/button_Register'))

WebUI.click(findTestObject('Page_DemoShop/button_Buy now'))

WebUI.click(findTestObject('Page_DemoShop/button_Direct payment'))

WebUI.click(findTestObject('Object Repository/Page_Payment Gateway/Option Payment_Test Bank 2'))

WebUI.setText(findTestObject('Object Repository/Page_Log in - BankSystem/input_Email'), 'citrasekaripb@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Log in - BankSystem/input_Password'), 'btDIBsWC4+X5P/BhQFnqDA==')

WebUI.click(findTestObject('Object Repository/Page_Log in - BankSystem/button_Log in'))

WebUI.click(findTestObject('Object Repository/Page_Confirm payment - BankSystem/button_Close'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Confirm payment - BankSystem/select_BRI BNI'), '1f8e339f-de0e-43fc-ab58-84a743a7b4aa', 
    true)

WebUI.click(findTestObject('Object Repository/Page_Confirm payment - BankSystem/button_Pay 800'))

WebUI.delay(4)

String product = WebUI.getText(findTestObject('Object Repository/Page_DemoShop/h4_Computer mouse'))

WebUI.verifyMatch(product, 'Computer mouse', true)

String price = WebUI.getText(findTestObject('Object Repository/Page_DemoShop/p_8.00'))

WebUI.verifyMatch(price, '€8.00', true)

WebUI.closeBrowser()

