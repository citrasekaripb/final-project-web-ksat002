import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl('https://banksystem-demoshop.herokuapp.com/')

WebUI.click(findTestObject('Page_DemoShop/a_Register'))

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Email'), 'latarot166@econeom.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Password'), 'QJblfja5Cso=')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_DemoShop/input_Confirm password'), 'QJblfja5Cso=')

WebUI.click(findTestObject('Page_DemoShop/button_Register'))

WebUI.click(findTestObject('Page_DemoShop/button_Buy now'))

WebUI.click(findTestObject('Page_DemoShop/button_Pay by card'))

WebUI.setText(findTestObject('Page_DemoShop/input_Card number_Number'), '84188484895959')

WebUI.delay(4)

WebUI.verifyElementText(findTestObject('Object Repository/Page_DemoShop/span_Invalid card number'), 'Invalid card number')

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Cardholder name_Name'), 'hana')

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Expiration date_ExpiryDate'), '12/03')

WebUI.setText(findTestObject('Object Repository/Page_DemoShop/input_Security code_SecurityCode'), '848')

WebUI.click(findTestObject('Object Repository/Page_DemoShop/button_Pay'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_DemoShop/li_Invalid card number'), 'Invalid card number')

WebUI.closeBrowser()

